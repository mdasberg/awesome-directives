'use strict';

angular.module('awesome-directives').directive('piggyHandle', [function () {
    return {
        restrict: 'E',
        transclude: true,
        templateUrl: '/js/components/piggyHandle/piggy.html'
    };
}]);