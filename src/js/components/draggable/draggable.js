(function () {
    'use strict';

    function Draggable($window, $document, $parse) {
        return {
            restrict: 'A',
            controller: 'adDraggableCtrl',
            controllerAs: 'ctrl',
            link: function (scope, element, attrs, ctrl) {
                /**
                 * Get the coordinates from the given event.
                 * @param event The event.
                 * @returns {{x: *, y: *}}
                 */
                function getCoordinates(event) {
                    return {
                        x: event.pageX,
                        y: event.pageY
                    }
                }

                /**
                 * Register or unregister document listeners so they are only enabled
                 * when the mouse is down.
                 * @param action on - off
                 */
                function setListeners(action) {
                    $document[action]('mousemove', move);
                    $document[action]('mouseup', up);
                }

                /**
                 * Handle start dragging.
                 * @param event The event.
                 */
                var down = function (event) {
                    event.preventDefault();
                    var coordinates = getCoordinates(event);
                    setListeners('on');
                    ctrl.start(coordinates);
                    if (attrs.onDragStart) {
                        $parse(attrs.onDragStart)(scope)(coordinates);
                    }
                };

                /**
                 * Handle dragging.
                 * @param event The event.
                 */
                var move = function (event) {
                    event.preventDefault();
                    ctrl.setOffset(getCoordinates(event));

                    if (attrs.onDrag) {
                        $parse(attrs.onDrag)(scope)(ctrl.offset);
                    }
                    if (!attrs.override) {
                        ctrl.drag();
                    }
                };

                /**
                 * Handle the stop dragging.
                 * @param event The event.
                 */
                var up = function (event) {
                    event.preventDefault();
                    var coordinates = getCoordinates(event);
                    setListeners('off');
                    if (attrs.onDragEnd) {
                        $parse(attrs.onDragEnd)(scope)(coordinates);
                    }
                };

                /**
                 * Attrs override:if we do the moving: make sure position is set to something ;-)
                 */
                if (angular.isUndefined(attrs.override)) {
                    if (element.css('position') === 'static' || element.css('position') === '') {
                        element.css({position: 'relative'});
                    }
                }
                element.bind('mousedown', down);
            }
        };
    }

    Draggable.$inject = ['$window', '$document', '$parse']

    angular.module('awesome-directives').
        directive('adDraggable', Draggable);
})();