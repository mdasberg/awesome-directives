(function () {
    'use strict';

    function DraggableCtrl($element) {
        var vm = this;

        /**
         * Start dragging.
         * @param elCoordinates The current coordinates of the element.
         * Also init position: has to be the current position of the element
         */
        vm.start = function (elCoordinates) {
            vm.coordinates = elCoordinates;
            vm.position = {
                top: $element.css('top') !== '' ? parseInt($element.css('top').replace('px', '')) : 0,
                left: $element.css('left') !== '' ? parseInt($element.css('left').replace('px', '')) : 0
            };
        };

        /**
         * Sets the offset coordinates.
         * @param offsetCoordinates The offset coordinates.
         */
        vm.setOffset = function(offsetCoordinates) {
            vm.offset = {
                x: offsetCoordinates.x - vm.coordinates.x,
                y: offsetCoordinates.y - vm.coordinates.y
            };
        };

        /**
         * Drag. Set the css of the element
         */
        vm.drag = function () {
            $element.css({
                top: (vm.position.top + vm.offset.y) + 'px',
                left: (vm.position.left + vm.offset.x) + 'px'
            });
        };
    }

    DraggableCtrl.$inject = ['$element'];

    angular.module('awesome-directives').
        controller('adDraggableCtrl', DraggableCtrl);
})();