'use strict';

/**
 * just a template and title
 */
angular.module('awesome-directives').directive('popover', [function () {
    return {
        restrict: 'E',
        transclude: true,
        scope:true,
        link: function(scope, element, attrs){
            scope.title = attrs.title;
        },
        templateUrl: '/js/components/popover/popover.html'
    };
}]);