'use strict';

/**
 * A handle allows the user to choose a value in the slider's range. The user can click and drag the handle over the slider bar to alter the value.
 * the value of the model should _always_ be set. if it is undefined, where should the handle be positioned?
 * In practice, you will probably want to set it to the minimum value. However, the slider does not set this value automatically to avoid tinkering with your model
 */
angular.module('awesome-directives').directive('adSliderHandle', function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: {},
        require: ['ngModel', '^adSlider'],
        link: function(scope, element, attrs, controllers) {
            var ngModelCtrl = controllers[0],
                adSlider = controllers[1],
                handleId = adSlider.addHandle(),
                handle = adSlider.handles[handleId],
                startValue;

            function calcOffset(){
                return Math.max(
                    Math.min(
                        // A value of min is 0%, a value of max is 100%
                        ((ngModelCtrl.$modelValue - adSlider.getMin()) * 100) / (adSlider.getMax() - adSlider.getMin()),
                        100), 0);
                }

            function roundToStep(viewValue) {
                // do not set invalide value's to the model...
                viewValue = Math.max(Math.min(viewValue, handle.max), handle.min);
                var remainder = viewValue % adSlider.getStep();
                viewValue = viewValue - remainder;
                if(remainder > (adSlider.getStep() / 2)){
                    viewValue = viewValue + adSlider.getStep();
                }
                return viewValue;
            }

            /**
             * implement step-snapping by adding model parser. (cool)
             * we manually update the value in the controller...
             * @todo can we bind from the controller to the value?
             */
            ngModelCtrl.$parsers.push(function(viewValue){
                adSlider.updateHandle(handleId, ngModelCtrl.$modelValue, scope.offset);
                return roundToStep(viewValue);
            });

            /**
             * when model changes, also change the view: Function is called automatic by angular on model-change
             * Get handle offset to notify the rest
             */
            ngModelCtrl.$render = function(){
                scope.offset = calcOffset();
                // notifiy handle in controller of updates
                // @ todo binding would be cool here...
                adSlider.updateHandle(handleId, ngModelCtrl.$modelValue, scope.offset);
            };
          
            scope.startDrag = function(){
                startValue = ngModelCtrl.$modelValue;
            };

            scope.drag = function(offset){
                scope.offset = calcOffset();
                ngModelCtrl.$setViewValue(startValue + (offset.x / adSlider.getWidth()) * (adSlider.getMax() - adSlider.getMin()), 'handleDrag');
            };
        },
        templateUrl: '/js/components/slider/sliderHandle.html'
    };
});