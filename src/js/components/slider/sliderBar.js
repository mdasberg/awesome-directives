'use strict';

/**
 * Slider bar has a filled range between the slider
 * Resposibilities:
 * - setting the filled portion
 * - handle clicks to directily go to value
 */
angular.module('awesome-directives').directive('adSliderBar', [function () {
    return {
        require: '^adSlider',
        restrict: 'E',
        scope: {
            segments: '='
        },
        transclude: true,
        link: function(scope, element, attrs,adSliderCtrl){
            var segments =  new Array(((adSliderCtrl.getMax() - adSliderCtrl.getMin()) / adSliderCtrl.getStep())+1);
            var highIndex = (adSliderCtrl.handles.length === 1) ? 0: 1;

            scope.segments = (!scope.segments) ? segments : scope.segments;
            scope.segmentWidth = 100 / (scope.segments.length);
            scope.barWidth = 100+ (100 / (scope.segments.length-1));
            scope.fillStart = scope.fillWidth= 0;

            scope.$watch(function(){
                if(highIndex === 0 ) {
                    scope.fillWidth = adSliderCtrl.handles[0].offset;
                }
                else {
                    scope.fillStart = adSliderCtrl.handles[0].offset;
                    scope.fillWidth = adSliderCtrl.handles[1].offset - scope.fillStart;
                }
            });
        },
        templateUrl: '/js/components/slider/sliderBar.html'
    };
}]);