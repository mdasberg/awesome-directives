'use strict';

angular.module('awesome-directives').controller('adSliderCtrl', ['$element', function($element){
    var vm = this;
    vm.handles = [];

    vm.addHandle = function() {
        var index = vm.handles.push({})- 1,
            handle = vm.handles[index],
            otherHandle = (index === 1) ? vm.handles[0] : vm.handles[1];

        setBounds(handle, otherHandle);

        if(handle && otherHandle) {
            if(handle.handleValue < otherHandle.handleValue){
                otherHandle.handleHigh = handle.handleLow = true;
            } else {
                otherHandle.handleLow = handle.handleHigh = true;
            }
        }
        return index;
    };

    vm.updateHandle = function(handleId, value, offset) {
        var otherHandle = (handleId === 1) ? vm.handles[0] : vm.handles[1];
        vm.handles[handleId].handleValue = value;
        vm.handles[handleId].offset = offset;
        setBounds(vm.handles[handleId], otherHandle);
    };

    /**
     * expose some shared scope vars
     */
    vm.getMax = function(){
        return parseFloat(vm.max);
    };

    vm.getMin = function(){
        return parseFloat(vm.min);
    };

    vm.getStep = function(){
        return parseFloat(vm.step);
    };

    vm.getWidth = function(){
        return $element[0].firstChild.offsetWidth;
    };

    /**
     * @param handle the handle to set the bounds for
     * @param otherHandle: [optional] set max accriding to this handle
     */
    function setBounds(handle, otherHandle) {
        handle.max = (otherHandle && handle.handleLow) ? otherHandle.handleValue : parseFloat(vm.max);
        handle.min = (otherHandle && handle.handleHigh) ? otherHandle.handleValue : parseFloat(vm.min);
    }
}]);