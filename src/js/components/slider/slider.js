'use strict';

/**
 * @description
 * A slider is a space efficient form control element that provides a user a means of input within a predefined range.
 * It provides the user with a cue of possible values, and a means to roughly pick a desired value.
 * This directive is merely the container that allows the setting of the slider's properties (e.g. range and step size).
 * Functionality can be added by nesting related directives: this should include at least {@link awesome-directives.directive:ad-slider-bar a slider bar} and {@link awesome-directives.directive:ad-slider-handle a handle}
 * that allows the user to actually choose a value. When the slider includes two handles, the user can use them to specify a range, whereby the handles represent the range's lower and higher bounds.
 */
angular.module('awesome-directives').directive('adSlider', [function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: {},
        controller: 'adSliderCtrl',
        controllerAs: 'ctrl',
        bindToController: {
            min: '@',
            max: '@',
            step: '@'
        },
        templateUrl: '/js/components/slider/slider.html'
    };
}]);