var grunt = require('grunt');
var path = require('path');

module.exports = function (config) {
    config.set({
            basePath: '../',
            frameworks: ['jasmine'],
            files: [
                'node_modules/jquery/dist/jquery.min.js',
                'node_modules/angular/angular.min.js',
                'node_modules/angular-mocks/angular-mocks.js',
                'src/**/*.js',
                'test/mocks/**/*.js',
                'test/unit/**/*.js',
                '**/*.html'
            ],
            exclude: [],
            plugins: [
                'karma-jasmine',
                'karma-junit-reporter',
                'karma-coverage',
                'karma-phantomjs-launcher',
                'karma-ng-html2js-preprocessor'
            ],
            preprocessors: {
                '**/*.html': ['ng-html2js'],
                'src/**/*.js': 'coverage'
            },
            ngHtml2JsPreprocessor: {
                stripPrefix: 'src',
                moduleName: 'templates'
            },
            reporters: ['progress', 'coverage', 'junit'],
            junitReporter: {
                outputFile: 'results/karma/results.xml'
            },
            coverageReporter: {
                reporters: [
                    {type: 'lcov', dir: 'results/karma/coverage'},
                    {type: 'html', dir: 'results/karma/coverage'},
                    {type: 'json', dir: 'results/karma/coverage'}
                ]
            },
            colors: true,
            logLevel: config.LOG_INFO,
            autoWatch: true,
            browsers: ['PhantomJS'],
            captureTimeout: 10000,
            singleRun: false
        }
    );
};
