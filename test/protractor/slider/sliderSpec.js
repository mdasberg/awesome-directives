'use strict';

var SliderPO = require("./../po/SliderPO");

describe('Slider', function() {

    beforeEach(function () {
        browser.get('/slider/slider.html');
    });
    
    describe('A default slider', function (){
        var slider;
        beforeEach(function() {
            slider = new SliderPO(element(by.id('singleSlider')));    
        });
        
        it('should slide right when it is not at the end', function() {
            expect(slider.currentStep).toBe(3);
            browser.sleep(100);
            slider.slideRight();
            expect(slider.currentStep).toBe(4);
        });

        it('should not slide right when at the end', function() {
            expect(slider.currentStep).toBe(3);
            browser.sleep(100);
            slider.slideRight();
            expect(slider.currentStep).toBe(4);
            browser.sleep(100);
            slider.slideRight();
            expect(slider.currentStep).toBe(5);
            
            // now at last position!!!
            browser.sleep(100);
            slider.slideRight();
            expect(slider.currentStep).toBe(5);
        });

        it('should slide left when it is not at the start', function() {
            expect(slider.currentStep).toBe(3);
            browser.sleep(100);
            slider.slideLeft();
            expect(slider.currentStep).toBe(2);
        });

        it('should not slide left when at the start', function() {
            expect(slider.currentStep).toBe(3);
            browser.sleep(100);
            slider.slideLeft();
            expect(slider.currentStep).toBe(2);
            browser.sleep(100);
            slider.slideLeft();
            expect(slider.currentStep).toBe(1);
            
            // now at the fist position!!!
            browser.sleep(100);
            slider.slideLeft();
            expect(slider.currentStep).toBe(1);
        });
    });

});