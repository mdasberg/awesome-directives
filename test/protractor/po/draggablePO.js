
var DraggablePO = function(el) {
    this.container = el;
    this.originalLocation = el.getLocation();
};

DraggablePO.prototype = Object.create({}, {
    dragTo: {
        value: function (offsetX, offsetY) {
            var deferred = protractor.promise.defer();
            
            browser.actions().
                mouseDown(this.container).
                mouseMove({x:offsetX,y:offsetY}).
                mouseUp().
                perform();

            return deferred.promise;
        }
    },
    hasMovedTo: {
        value: function(offsetX, offsetY) {
            var currentLocation = this.container.getLocation();
            return this.originalLocation.then(function(original) {
                return currentLocation.then(function(actual) {
                    return (original.x + offsetX === actual.x) && (original.y + offsetY === actual.y);
                });
            });
        }
    }
});

module.exports = DraggablePO;