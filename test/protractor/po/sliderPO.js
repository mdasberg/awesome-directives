var SliderPO = function (el) {
    this.container = el;

    this.sliderHandle = this.container.
        all(by.css('ad-slider-handle[ng-model="slider.sliderValue"]')).
        first().element(by.css('div[ad-draggable=""]'));

    this.originalLocation = el.getLocation();
};
SliderPO.prototype = Object.create({}, {
    currentStep: {
        get: function () {
            var sliderHandleLocation = this.sliderHandle.getLocation();
            var stepWidth = this.stepWidth();

            return sliderHandleLocation.then(function (sLoc) {
                return stepWidth.then(function (sWidth) {
                    var step = sLoc.x / sWidth;
                    return step < 1 ? 1 : Math.round(step);
                });
            });
        }
    },
    stepWidth: {
        value: function () {
            var size = this.container.getSize();
            var max = this.container.getAttribute('max');
            var min = this.container.getAttribute('min');

            return size.then(function (size) {
                return max.then(function (max) {
                    return min.then(function(min){
                        return size.width / (max-min);
                    })
                });
            });
        }
    },
    slideRight: {
        value: function () {
            var deferred = protractor.promise.defer();
            var sliderHandle = this.sliderHandle;
            this.stepWidth().then(function (width) {

                browser.actions().
                    mouseDown(sliderHandle).
                    mouseMove({x: width - 1, y: 0}).
                    mouseMove({x: 1, y: 0}). // trigger view update
                    mouseUp().
                    perform();
            });
            return deferred.promise;
        }
    },
    slideLeft: {
        value: function () {
            var deferred = protractor.promise.defer();
            var sliderHandle = this.sliderHandle;
            this.stepWidth().then(function (width) {
                browser.actions().
                    mouseDown(sliderHandle).
                    mouseMove({x: -(width - 1), y: 0}).
                    mouseMove({x: -1, y: 0}). // trigger view update
                    mouseUp().
                    perform();
            });
            return deferred.promise;
        }
    }
});

module.exports = SliderPO;