/* globals jasmine */

angular.module('sliderModule', ['awesome-directives']);

/* jasmine specs for the slider, slider-bar, slider-handle  */
describe('Slider', function () {
    'use strict';

    var element, scope, controller, handleScope;

    var findBar = function(element){
        return element.find('ad-slider-bar');
    };

    var findHandle = function(element){
        return element.find('ad-slider-bar').find('ad-slider-handle');
    };

    beforeEach(function () {
        module('sliderModule');
        module('templates');
    });

    describe('with only a bar and a handle', function () {

        beforeEach(function () {
            inject(function ($rootScope, $compile) {
                scope = $rootScope.$new();
                scope.min = 2.5;
                scope.max = 7.0;
                scope.step = 0.5;
                scope.model = {
                    value: 3.5
                };
                element = angular.element(
                    '<ad-slider min="{{min}}" max="{{max}}" step="{{step}}">' +
                    '   <ad-slider-bar>' +
                    '       <ad-slider-handle ng-model="model.value"></ad-slider-handle>' +
                    '   </ad-slider-bar>' +
                    '</ad-slider>'
                );

                $compile(element)(scope);
                controller = element.controller('adSlider');

                scope.$digest();
                handleScope = findHandle(element).isolateScope();
                spyOn(handleScope, 'startDrag');
                handleScope.$digest();
            });
        });

        it('should have an outer container', function () {
            expect(element.find('div').attr('class')).toContain('slider');
        });

        it('should have a slider-track inside', function () {
            expect(findBar(element).find('div').attr('class')).toContain('slider-track');
        });

        it('should have an element to represent the chosen range', function () {
            expect(findBar(element).find('div').find('div').attr('class')).toMatch('slider-selection');
        });

        it('should have a draggable handle that is overridden', function () {
            expect(findHandle(element).find('div').attr('ad-draggable')).toBeDefined();
            expect(findHandle(element).find('div').attr('override')).toBeDefined();
        });

        it('should have the correct number of segments ', function () {
            expect(findBar(element).isolateScope().segments.length).toBe(((7-2.5)/0.5)+1); // (max-min)/step
        });

        describe('; its handle', function(){
            beforeEach(function(){
                controller = element.controller('adSlider');
            });

            it('should register itself with the slider controller', function () {
                expect(controller.handles.length).toBe(1);
            });
            it('should correctly calculate its offset', function () {
                expect(controller.handles[0].offset).toBe(((3.5-2.5)*100)/(7-2.5)); // ((value-min)*100)/(max-min)
            });

            it('should report correctly about its arguments', inject(function ($compile) {
                expect(controller.getMin()).toBe(2.5);
                expect(controller.getStep()).toBe(0.5);
                expect(controller.getMax()).toBe(7.0);
            }));
            it('should not update the model with viewValuenull', function () {
                var modelController = findHandle(element).controller('ngModel');
                modelController.$setViewValue('2.5');
                expect(modelController.$modelValue).toBe(2.5);
                modelController.$setViewValue(null);
                expect(modelController.$modelValue).toBe(2.5);
            });

            it('should snap values to the closest applicable step', function () {
                var modelController = findHandle(element).controller('ngModel');
                modelController.$setViewValue('2.625');
                expect(modelController.$modelValue).toBe(2.5);
                modelController.$setViewValue('2.751');
                expect(modelController.$modelValue).toBe(3.0);
                modelController.$setViewValue('6.957');
                expect(modelController.$modelValue).toBe(7.0);
                modelController.$setViewValue('6.651');
                expect(modelController.$modelValue).toBe(6.5);
                modelController.$setViewValue('7.957');
                expect(modelController.$modelValue).toBe(7.0); // Beyond the higher boundary - snap to max
                modelController.$setViewValue('-6.957');
                expect(modelController.$modelValue).toBe(2.5); // Below the lower boundary - snap to min
            });

        });
    });

    describe('with two handles', function () {
        var initialiseSlider = function(min, max, step, lowerBound, higherBound){
            inject(function ($rootScope, $compile) {
                scope = $rootScope.$new();
                scope.min = 2.5;
                scope.max = 7.0;
                scope.step = 0.5;
                scope.model = {
                    lowerBound: 3.0,
                    higherBound: 6.0
                };
                element = angular.element(
                        '<ad-slider min="{{min}}" max="{{max}}" step="{{step}}">' +
                        '   <ad-slider-bar>' +
                        '       <ad-slider-handle ng-model="model.lowerBound">' +
                        '       </ad-slider-handle>' +
                        '       <ad-slider-handle ng-model="model.higherBound">' +
                        '       </ad-slider-handle>' +
                        '   </ad-slider-bar>' +
                        '</ad-slider>'
                );
                $compile(element)(scope);
                scope.$digest();
            });
        };

        var handles;

        beforeEach(function () {
            initialiseSlider();
            handles = findHandle(element);
            controller = element.controller('adSlider');
        });

        it('should be know there are two handles', function () {
            expect(controller.handles.length).toBe(2);
        });

        it('should determine its maximum and minimum values based on the other handle', function(){
            expect(controller.handles[0].min).toBe(controller.getMin());
            expect(controller.handles[1].max).toBe(controller.getMax());
            expect(controller.handles[1].min).toBe(controller.handles[0].handleValue);
        });
    });
});